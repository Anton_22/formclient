﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppFormClient
{
    public class ListCitizen : ObservableCollection<Citizen>
    {
        public ListCitizen()
        {
            using (var context = new ClientDBEntities())
            {
                var query = from citizen in context.Citizens
                            orderby citizen.FirstName
                            select citizen;
                foreach (var c in query)
                    this.Add(c);
            }

            using (var context = new ClientDBEntities())
            {
                var query = from citizen in context.Citizens
                            orderby citizen.SecondName
                            select citizen;
                foreach (var c in query)
                    this.Add(c);
            }
            using (var context = new ClientDBEntities())
            {
                var query = from citizen in context.Citizens
                            orderby citizen.LastName
                            select citizen;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}

