﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppFormClient
{
    public class ListPerson : ObservableCollection<Person>
    {
        public ListPerson()
        {
            using (var context = new ClientDBEntities())
            {
                var query = from pers in context.People
                            orderby pers.Shifer
                            select pers;
                foreach (var c in query)
                    this.Add(c);
            }
            using (var context = new ClientDBEntities())
            {
                var query = from pers in context.People
                            orderby pers.Inn
                            select pers;
                foreach (var c in query)
                    this.Add(c);
            }
            using (var context = new ClientDBEntities())
            {
                var query = from pers in context.People
                            orderby pers.Type
                            select pers;
                foreach (var c in query)
                    this.Add(c);
            }
            using (var context = new ClientDBEntities())
            {
                var query = from pers in context.People
                            orderby pers.Data
                            select pers;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}
