﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections.ObjectModel; namespace WpfAppFormClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Citizen> Citizens;
        ObservableCollection<Person> Persons;
        ObservableCollection<Document> Documents;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnLoad_Click_1(object sender, RoutedEventArgs e)
        {
            using (var context = new ClientDBEntities())
            {
                var query = from Citizen in context.Citizens
                            select Citizen;
                if (query.Count() != 0)
                {
                    Citizens = new ObservableCollection<Citizen>();
                    foreach (var c in query)
                        Citizens.Add(c);

                    lvCitizen.ItemsSource = Citizens;
                }
            }
        }

        private void btnLoad1_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ClientDBEntities())
            {
                var query = from Person in context.People

                            select Person;
                if (query.Count() != 0)
                {
                    Persons = new ObservableCollection<Person>();
                    foreach (var c in query)
                        Persons.Add(c);

                    lvPerson.ItemsSource = Persons;
                }
            }

        }

        private void btnLoad2_Click(object sender, RoutedEventArgs e)
        {

            using (var context = new ClientDBEntities())
            {
                var query = from Document in context.Documents

                            select Document;
                if (query.Count() != 0)
                {
                    Documents = new ObservableCollection<Document>();
                    foreach (var c in query)
                        Documents.Add(c);

                    lvDocument.ItemsSource = Documents;
                }

            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Citizen editCiti = (Citizen)lvPerson.SelectedItem;

            EditCitizenView editCitizenView = new EditCitizenView();
            editCitizenView.Title = "Редактирование данных по клиенту";
            editCitizenView.DataContext = editCiti;
            editCitizenView.ShowDialog();

            if (editCitizenView.DialogResult == true)
            {
                using (var context = new ClientDBEntities())
                {
                    Citizen citi = context.Citizens.Find(editCiti.Id);
                    if (citi.FirstName != editCiti.FirstName)
                        citi.FirstName = editCiti.FirstName.Trim();
                    if (citi. SecondName!= editCiti.SecondName)
                        citi.SecondName = editCiti.SecondName.Trim();
                    if (citi.LastName != editCiti.LastName)
                        citi.LastName = editCiti.LastName.Trim();
                    if (citi.Number != editCiti.Number)
                        citi.Number = editCiti.Number.GetValueOrDefault();
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }


        }
        /// Редактироание данных 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEdit_Click(object sender, RoutedEventArgs e)
        {
            Person editPerson = (Person)lvPerson.SelectedItem;

            EditPersonView editPersonView = new EditPersonView();
            editPersonView.Title = "Редактироание данных по лицам";
            editPersonView.DataContext = editPerson;
            editPersonView.ShowDialog();

            if (editPersonView.DialogResult == true)
            {
                using (var context = new ClientDBEntities())
                {
                    Person person = context.People.Find(editPerson.Id);

                    if (person.Shifer != editPerson.Shifer)
                        person.Shifer = editPerson.Shifer.Trim();
                    if (person.Type != editPerson.Type)
                        person.Type = editPerson.Type.Trim();
              

                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("\nОшибка редактирования данных!\n" + ex.Message, "Предупреждение");
                    }
                }
            }
        }
        

        ///Удаление клиента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btDeleteCiti_Click(object sender, RoutedEventArgs e)
        {
            Citizen delCitizen = (Citizen)lvCitizen.SelectedItem;

            using (var context = new ClientDBEntities())
            {
                // Поиск в контексте удаляемого 
                Citizen delCiti = context.Citizens.Find(delCitizen.Id);

                if (delCiti != null)
                {
                    MessageBoxResult result = MessageBox.Show("Удалить данные по клиенту: \n" + delCiti.LastName + "  " + delCiti.FirstName,
                  "Предупреждение", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            context.Citizens.Remove(delCiti);
                            context.SaveChanges();
                            Citizens.Remove(delCitizen);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("\nОшибка удаления данных!\n" + ex.Message, "Предупреждение");
                        }
                    }
                }
            }
        }
    }
}
