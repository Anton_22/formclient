﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppFormClient
{
    /// <summary>
    /// Логика взаимодействия для EditDocumentView.xaml
    /// </summary>
    public partial class EditDocumentView : Window
    {
    
        public EditDocumentView()
        {
            InitializeComponent();
        }
        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
