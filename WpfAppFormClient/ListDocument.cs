﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppFormClient
{
    public class ListDocument : ObservableCollection<Document>
    {
        public ListDocument()
        {
            using (var context = new ClientDBEntities())
            {
                var query = from docu in context.Documents
                            orderby docu.Name
                            select docu;
                foreach (var c in query)
                    this.Add(c);
            }
        }
    }
}
