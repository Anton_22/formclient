﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppFormClient
{
    /// <summary>
    /// Логика взаимодействия для EditCitizen.xaml
    /// </summary>
    public partial class EditCitizenView : Window
    {
        public EditCitizenView()
        {
            InitializeComponent();
        }
     
            ObservableCollection<Document> documents;

            public ObservableCollection<Document> Documents
            {
                get { return documents; }
                set { documents = value; }
            }
            ObservableCollection<Document> document;

        ObservableCollection<Person> persons;

        public ObservableCollection<Person> Persons
            {
                get { return persons; }
                set { persons = value; }
            }
  
        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
